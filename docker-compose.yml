version: "3.7"

x-env: &env
  TLS_GROUP_ROOT_CERT: /certs/organization/nlx-pki-root.crt
  TLS_ROOT_CERT: /certs/internal/internal-root.crt
  TLS_CERT: /certs/internal/internal-cert.crt
  TLS_KEY: /certs/internal/internal-cert.key
  GROUP_ID: fsc-demo

x-certs: &certs
  - ./pki/organization/ca/root.crt:/certs/organization/nlx-pki-root.crt:ro
  - ./pki/organization/certs/org.crt:/certs/organization/nlx-pki-org-cert.crt:ro
  - ./pki/organization/certs/org.key:/certs/organization/nlx-pki-org-key.key:ro
  - ./pki/internal/ca/intermediate_ca.pem:/certs/internal/internal-root.crt:ro
  - ./pki/internal/certs/internal-cert.pem:/certs/internal/internal-cert.crt:ro
  - ./pki/internal/certs/internal-cert-key.pem:/certs/internal/internal-cert.key:ro

services:
  controller:
    image: nlxio/fsc-controller:v0.11.0
    restart: unless-stopped
    ports:
      - 8080:3001
    volumes: *certs
    environment:
      <<: *env
      LISTEN_ADDRESS_UI: 0.0.0.0:3001
      LISTEN_ADDRESS_INTERNAL: 0.0.0.0:443
      LISTEN_ADDRESS_API: 0.0.0.0:444
      MANAGER_ADDRESS_INTERNAL: https://manager:443
      DIRECTORY_ADDRESS: https://directory.demo.fsc.nlx.io:8443
      STORAGE_POSTGRES_DSN: "postgresql://postgres:postgres@postgres:5432/nlx_controller?sslmode=disable&connect_timeout=2"
    depends_on:
      - manager
      - migrations-controller
      - postgres

  manager:
    image: nlxio/fsc-manager:v0.11.0
    restart: unless-stopped
    ports:
      - 8443:8443
    volumes: *certs
    environment:
      <<: *env
      TLS_GROUP_CERT: /certs/organization/nlx-pki-org-cert.crt
      TLS_GROUP_KEY: /certs/organization/nlx-pki-org-key.key
      TLS_GROUP_TOKEN_CERT: /certs/organization/nlx-pki-org-cert.crt
      TLS_GROUP_TOKEN_KEY: /certs/organization/nlx-pki-org-key.key
      TLS_GROUP_CONTRACT_CERT: /certs/organization/nlx-pki-org-cert.crt
      TLS_GROUP_CONTRACT_KEY: /certs/organization/nlx-pki-org-key.key
      LISTEN_ADDRESS_INTERNAL: 0.0.0.0:443
      LISTEN_ADDRESS_EXTERNAL: 0.0.0.0:8443
      CONTROLLER_API_ADDRESS: https://controller:443
      TX_LOG_API_ADDRESS: https://txlog-api:8443
      SELF_ADDRESS: "${MANAGER_SELF_ADDRESS}"
      DIRECTORY_PEER_MANAGER_ADDRESS: https://directory.demo.fsc.nlx.io:8443
      DIRECTORY_PEER_ID: "12345678901234567899"
      STORAGE_POSTGRES_DSN: "postgresql://postgres:postgres@postgres:5432/nlx_manager?sslmode=disable&connect_timeout=2"
    depends_on:
      - migrations-manager
      - txlog-api
      - postgres

  outway:
    image: nlxio/fsc-outway:v0.11.0
    restart: unless-stopped
    ports:
      - 8081:8080
    environment:
      <<: *env
      TLS_GROUP_CERT: /certs/organization/nlx-pki-org-cert.crt
      TLS_GROUP_KEY: /certs/organization/nlx-pki-org-key.key
      LISTEN_ADDRESS: 0.0.0.0:8080
      CONTROLLER_API_ADDRESS: https://controller:443
      TX_LOG_API_ADDRESS: https://txlog-api:8443
      MANAGER_INTERNAL_ADDRESS: https://manager:443
      OUTWAY_NAME: Outway-01
    volumes: *certs
    depends_on:
      - controller
      - txlog-api
      - manager

  inway:
    image: nlxio/fsc-inway:v0.11.0
    restart: unless-stopped
    ports:
      - 443:443
    environment:
      <<: *env
      TLS_GROUP_CERT: /certs/organization/nlx-pki-org-cert.crt
      TLS_GROUP_KEY: /certs/organization/nlx-pki-org-key.key
      LISTEN_ADDRESS: 0.0.0.0:443
      INWAY_NAME: Inway-01
      SELF_ADDRESS: "${INWAY_SELF_ADDRESS}"
      CONTROLLER_API_ADDRESS: https://controller:443
      TX_LOG_API_ADDRESS: https://txlog-api:8443
      MANAGER_INTERNAL_ADDRESS: https://manager:443
    volumes: *certs
    depends_on:
      - controller
      - txlog-api
      - manager

  migrations-controller:
    image: nlxio/fsc-controller:v0.11.0
    restart: on-failure
    command:
      - /usr/local/bin/nlx-controller
      - migrate
      - up
    environment:
      <<: *env
      POSTGRES_DSN: "postgresql://postgres:postgres@postgres:5432/nlx_controller?sslmode=disable&connect_timeout=2"
    depends_on:
      - postgres

  migrations-manager:
    image: nlxio/fsc-manager:v0.11.0
    restart: on-failure
    command:
      - /usr/local/bin/nlx-manager
      - migrate
      - up
    environment:
      <<: *env
      POSTGRES_DSN: "postgresql://postgres:postgres@postgres:5432/nlx_manager?sslmode=disable&connect_timeout=2"
    depends_on:
      - postgres

  migrations-txlog-api:
    image: nlxio/fsc-txlog-api:v0.11.0
    restart: on-failure
    command:
      - /usr/local/bin/nlx-txlog-api
      - migrate
      - up
    environment:
      <<: *env
      POSTGRES_DSN: "postgresql://postgres:postgres@postgres:5432/nlx_txlog?sslmode=disable&connect_timeout=2"
    depends_on:
      - postgres

  txlog-api:
    image: nlxio/fsc-txlog-api:v0.11.0
    restart: unless-stopped
    volumes: *certs
    environment:
      <<: *env
      STORAGE_POSTGRES_DSN: "postgresql://postgres:postgres@postgres:5432/nlx_txlog?sslmode=disable&connect_timeout=2"
      LISTEN_ADDRESS: 0.0.0.0:8443
      LISTEN_ADDRESS_PLAIN: 0.0.0.0:8080
    depends_on:
      - migrations-txlog-api
      - postgres

  postgres:
    image: postgres:15-alpine
    restart: unless-stopped
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
    volumes:
      - ./scripts/init-databases.sh:/docker-entrypoint-initdb.d/init-databases.sh
      - postgres-data:/var/lib/postgresql/data

volumes:
  postgres-data:
