#!/bin/bash 
set -o errexit
set -o pipefail
set -o nounset

OUTPUT_PATH=./pki/organization

if [[ -f ".certs-initialized" ]]; then
    echo "Certificates are already initialized"
    echo "Exiting now"
    exit
fi

source ./scripts/init-internal-certs.sh
source ./scripts/init-organization-certs.sh

chmod 600 ./pki/organization/certs/org.key
chown 1001 ./pki/organization/certs/org.key
chmod 600 ./pki/internal/certs/internal-cert-key.pem
chown 1001 ./pki/internal/certs/internal-cert-key.pem

touch .certs-initialized

echo "Done"
